﻿using Microsoft.IdentityModel.Tokens;

namespace ApiGateway
{
    public static class Registrar
    {
        internal static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            return services.AddSingleton((IConfigurationRoot)configuration)
                .AddAuth(configuration);                ;
        }

        #region Auth

        private static IServiceCollection AddAuth(this IServiceCollection serviceCollection, IConfiguration configuration)
        {            
            string issuingAuthority = configuration.GetSection("Keycloak").Value!;
            const string validAudience = "account";

            serviceCollection.AddAuthentication().AddJwtBearer(jwtBearerOptions =>
            {
                jwtBearerOptions.Authority = issuingAuthority;
                jwtBearerOptions.RequireHttpsMetadata = false;
                jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidIssuer = issuingAuthority,
                    ValidAudience = validAudience,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true
                };
            });                       

            return serviceCollection;
        }

        #endregion
    }
}
