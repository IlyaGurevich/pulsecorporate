﻿using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQService.Interfaces;
using System.Text;
using System.Text.Json;

namespace RabbitMQService.Services
{
    public class RabbitMQConsumer<T>:IRabbitMQConsumer<T>, IDisposable
    {
        private readonly IConnection _connection;
        private readonly ConnectionFactory _connectionFactory;
        private readonly IModel _model;
        private readonly IConfiguration _configuration;

        private EventingBasicConsumer _eventingBasicConsumer;

        public RabbitMQConsumer()
        {
            _configuration = new ConfigurationBuilder()
                .AddJsonFile("rabbitsettings.json", optional: true, reloadOnChange: true)
                .Build();
            _connectionFactory = new ConnectionFactory() { HostName = _configuration["RabbitMQServices:HostName"], UserName = _configuration["RabbitMQServices:UserName"], Password = _configuration["RabbitMQServices:Password"] };
            _connection = _connectionFactory.CreateConnection();
            _model = _connection.CreateModel();
            _model.QueueDeclare(queue: _configuration["RabbitMQServices:QueueName"],
                               durable: false,
                               exclusive: false,
                               autoDelete: false,
                               arguments: null);
            _eventingBasicConsumer = new EventingBasicConsumer(_model);
        }

        public void Consume(List<Func<T, Task>> services)
        {
            _eventingBasicConsumer.Received += async (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = JsonSerializer.Deserialize<T>(Encoding.UTF8.GetString(body));

                foreach ( var service in services ) 
                { 
                    await service(message!);
                }
            };

            _model.BasicConsume(queue: _configuration["RabbitMQServices:QueueName"],
                                                autoAck: true,
                                                consumer: _eventingBasicConsumer);
        }

        public void Dispose()
        {
            _model.Close();
            _connection.Close();
        }

    }
}
