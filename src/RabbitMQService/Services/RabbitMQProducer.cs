﻿using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using RabbitMQService.Interfaces;
using System.Text;
using System.Text.Json;

namespace RabbitMQService.Services
{
    public class RabbitMQProducer<T>: IRabbitMQProducer<T>, IDisposable
    {
        private readonly IConnection _connection;
        private readonly ConnectionFactory _connectionFactory;
        private readonly IModel _model;
        private readonly IConfiguration _configuration;

        public RabbitMQProducer()
        {
            _configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .Build();
            _connectionFactory = new ConnectionFactory() { HostName = _configuration["RabbitMQServices:HostName"], UserName = _configuration["RabbitMQServices:UserName"], Password = _configuration["RabbitMQServices:Password"] };
            _connection = _connectionFactory.CreateConnection();
            _model = _connection.CreateModel();
            _model.QueueDeclare(queue: _configuration["RabbitMQServices:QueueName"],
                               durable: false,
                               exclusive: false,
                               autoDelete: false,
                               arguments: null);
            //_model.ExchangeDeclare(_configuration["RabbitMQServices:QueueName"], ExchangeType.Direct);
        }

        public void SendMessage(T objectDto)
        {
            var jsonBody = JsonSerializer.Serialize(objectDto);
            var utf8Body = Encoding.UTF8.GetBytes(jsonBody);

            _model.BasicPublish(exchange: "",
                           routingKey: _configuration["RabbitMQServices:QueueName"],
                           basicProperties: null,
                           body: utf8Body);
        }

        public void Dispose()
        {
            _model.Close();
            _connection.Close();
        }
    }
}
