﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQService.Interfaces
{
    public interface IRabbitMQConsumer<T>
    {
        void Consume(List<Func<T, Task>> action);
    }
}
