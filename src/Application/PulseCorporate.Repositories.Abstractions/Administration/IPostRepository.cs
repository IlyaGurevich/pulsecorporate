﻿using PulseCorporate.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Repositories.Abstractions.Administration
{
    /// <summary>
    /// Repositiry interface to manage Post entities.
    /// </summary>
    public interface IPostRepository: IRepository<Post>
    {
        Task<IEnumerable<Post>> GetForPaginationAsync(int page, int quantity);
    }
}
