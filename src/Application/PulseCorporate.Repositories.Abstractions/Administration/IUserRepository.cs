﻿using PulseCorporate.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Repositories.Abstractions.Administration
{

    public interface IUserRepository : IRepository<User>
    {
        Task<User?> GetByEmailAsync(string email, bool noTracking = true);

        Task<ICollection<User>> GetUsersById(IEnumerable<Guid> ids, bool noTracking = true);
    }
}
