﻿using MediatR;
using PulseCorporate.Domain.EntitiesDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Application.MeetingService.Commands
{
    public record UpdateMeetingAsyncCommand(Guid Id, MeetingDto Meeting, IEnumerable<Claim> UserClaims) : IRequest<MeetingDto>;       
}
