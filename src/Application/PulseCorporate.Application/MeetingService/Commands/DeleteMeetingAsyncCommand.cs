﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Application.MeetingService.Commands
{
    public record DeleteMeetingAsyncCommand(Guid Id, IEnumerable<Claim> UserClaims) : IRequest<Guid>;
}
