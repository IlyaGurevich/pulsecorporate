﻿using AutoMapper;
using MediatR;
using PulseCorporate.Application.PostService.Commands;
using PulseCorporate.Domain.Entities;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.Repositories.Abstractions.Administration;
using RabbitMQService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Application.PostService.CommandHandlers
{
    public class AddPostHandler : IRequestHandler<AddPostAsyncCommand, PostDto>
    {
        private readonly IUserRepository _userRepository;
        private readonly IPostRepository _postRepository;
        private readonly IMapper _mapper;
        private readonly IRabbitMQProducer<EmailDto> _rabbit;

        public AddPostHandler(IPostRepository postRepository, IUserRepository userRepository, IMapper mapper, IRabbitMQProducer<EmailDto> rabbit)
        {
            _postRepository = postRepository;
            _userRepository = userRepository;
            _mapper = mapper;
            _rabbit = rabbit;
        }

        public async Task<PostDto> Handle(AddPostAsyncCommand request, CancellationToken cancellationToken)
        {
            var email = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress")).Value;

            var existUser = await _userRepository.GetByEmailAsync(email);

            if (existUser is null)
            {
                var name = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname")).Value;
                var surName = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname")).Value;
                _userRepository.Add(new User { Name = name, Surname = surName, Email = email });
                await _userRepository.SaveChangesAsync(cancellationToken);

                existUser = await _userRepository.GetByEmailAsync(email);
            }

            request.Post.AuthorId = existUser!.Id;
            
            var newPost = _postRepository.Add(_mapper.Map<Post>(request.Post));
            await _postRepository.SaveChangesAsync(cancellationToken);

            _rabbit.SendMessage(new EmailDto(email, "Add post", $"Add post name - {request.Post.Name}, description - {request.Post.Description}"));

            newPost.Author = existUser;

            return _mapper.Map<PostDto>(newPost);
        }
    }
}
