﻿using MediatR;
using PulseCorporate.Domain.EntitiesDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Application.PostService.Queries
{
    public record GetPostsAsyncQuery() : IRequest<IEnumerable<PostDto>>;
}
