﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Application.PostService.Commands
{
    public record DeletePostAsyncCommand(Guid Id, IEnumerable<Claim> UserClaims) : IRequest<Guid>;
}
