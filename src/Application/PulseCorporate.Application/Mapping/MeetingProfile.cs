﻿using AutoMapper;
using PulseCorporate.Domain.Entities;
using PulseCorporate.Domain.EntitiesDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Application.Mapping
{
    public sealed class MeetingProfile: Profile
    {
        public MeetingProfile() 
        {
            CreateMap<MeetingDto, Meeting>()                
                .ForAllMembers(opt => opt.Condition((src, dest, srcMember) => srcMember is not null));
            CreateMap<Meeting, MeetingDto>()
                .ForMember(x => x.ParticipantsId, map => map.Ignore());
        }
    }
}
