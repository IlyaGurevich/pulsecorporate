﻿using AutoMapper;
using MediatR;
using PulseCorporate.Application.UserService.Queries;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.Repositories.Abstractions.Administration;


namespace PulseCorporate.Application.UserService.QueriesHandlers
{
    public class GetUsersHandler : IRequestHandler<GetUsersAsyncQuery, IEnumerable<UserDto>>
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        public GetUsersHandler(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }                

        public async Task<IEnumerable<UserDto>> Handle(GetUsersAsyncQuery request, CancellationToken cancellationToken)
        {
            return _mapper.Map<IEnumerable<UserDto>>(await _userRepository.GetAllAsync());
        }
    }
}
