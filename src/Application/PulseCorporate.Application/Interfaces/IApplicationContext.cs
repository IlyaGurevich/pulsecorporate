﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using PulseCorporate.Domain.Abstractions;

namespace PulseCorporate.Application.Interfaces;

/// <summary>
/// EntityFramework context interface.
/// </summary>
public interface IApplicationContext
{
    DbSet<T> Set<T>() where T : class;

    Task<int> SaveChangesAsync(CancellationToken cancellationToken);
}
