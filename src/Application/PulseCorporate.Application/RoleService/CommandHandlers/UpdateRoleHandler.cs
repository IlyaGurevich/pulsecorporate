﻿using AutoMapper;
using MediatR;
using PulseCorporate.Application.RoleService.Commands;
using PulseCorporate.Domain.Entities;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.Repositories.Abstractions.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Application.RoleService.CommandHandlers
{
    public class UpdateRoleHandler : IRequestHandler<UpdateRoleAsyncCommand, RoleDto>
    {
        private readonly IRoleRepository _roleRepository;
        private readonly IMapper _mapper;

        public UpdateRoleHandler(IRoleRepository roleRepository, IMapper mapper)
        {
            _roleRepository = roleRepository;
            _mapper = mapper;
        }

        public async Task<RoleDto> Handle(UpdateRoleAsyncCommand request, CancellationToken cancellationToken)
        {
            var existRole = await _roleRepository.GetByIdAsync(request.Id) ??
                throw new KeyNotFoundException($"Not found {nameof(Role)} with this id: {request.Id}");
            _mapper.Map(request.Role, existRole).Id = request.Id;

            _roleRepository.Update(_mapper.Map<Role>(existRole));
            await _roleRepository.SaveChangesAsync(cancellationToken);

            return request.Role;
        }
    }
}
