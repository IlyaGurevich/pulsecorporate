﻿using AutoMapper;
using MediatR;
using PulseCorporate.Application.RoleService.Commands;
using PulseCorporate.Domain.Entities;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.Repositories.Abstractions.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Application.RoleService.CommandHandlers
{
    public class DeleteRoleHandler : IRequestHandler<DeleteRoleAsyncCommand, Guid>
    {
        private readonly IRoleRepository _roleRepository;        

        public DeleteRoleHandler(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;            
        }

        public async Task<Guid> Handle(DeleteRoleAsyncCommand request, CancellationToken cancellationToken)
        {
            var existRole = await _roleRepository.GetByIdAsync(request.Id) ??
                throw new KeyNotFoundException($"Not found {nameof(Role)} with this id: {request.Id}");

            _roleRepository.Delete(existRole);
            await _roleRepository.SaveChangesAsync(cancellationToken);

            return request.Id;
        }
    }
}
