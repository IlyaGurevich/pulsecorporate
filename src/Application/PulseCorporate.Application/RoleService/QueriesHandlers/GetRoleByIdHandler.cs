﻿using AutoMapper;
using MediatR;
using PulseCorporate.Application.RoleService.Queries;
using PulseCorporate.Domain.Entities;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.Repositories.Abstractions.Administration;

namespace PulseCorporate.Application.RoleService.QueriesHandlers
{
    public class GetRoleByIdHandler : IRequestHandler<GetRoleByIdAsyncQuery, RoleDto>
    {
        private readonly IRoleRepository _roleRepository;
        private readonly IMapper _mapper;
        public GetRoleByIdHandler(IRoleRepository roleRepository, IMapper mapper)
        {
            _roleRepository = roleRepository;
            _mapper = mapper;
        }

        public async Task<RoleDto> Handle(GetRoleByIdAsyncQuery request, CancellationToken cancellationToken)
        {
            var existRole = await _roleRepository.GetByIdAsync(request.Id) ??
                throw new KeyNotFoundException($"Not found {nameof(Role)} with this id: {request.Id}");

            return _mapper.Map<RoleDto>(existRole);
        }
    }
}
