﻿using AutoMapper;
using MediatR;
using PulseCorporate.Application.RoleService.Queries;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.Repositories.Abstractions.Administration;
using RabbitMQService.Interfaces;

namespace PulseCorporate.Application.RoleService.QueriesHandlers
{
    public sealed class GetRolesHandler : IRequestHandler<GetRolesAsyncQuery, IEnumerable<RoleDto>>
    {
        private readonly IRoleRepository _roleRepository;
        private readonly IMapper _mapper;

        public GetRolesHandler(IRoleRepository roleRepository, IMapper mapper)
        {
            _roleRepository = roleRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<RoleDto>> Handle(GetRolesAsyncQuery request, CancellationToken cancellationToken)
        {
            var roles = await _roleRepository.GetAllAsync();
            return roles.Select(_mapper.Map<RoleDto>);
        }
    }
}
