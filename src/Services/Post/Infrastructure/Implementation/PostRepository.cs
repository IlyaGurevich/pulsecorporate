﻿using Microsoft.EntityFrameworkCore;
using Post.API.Application.Abstractions;
using Post.API.Domain.Entities;
using Post.API.Infrastructure.Interfaces;

namespace Post.API.Infrastructure.Implementation
{
    /// <summary>
    /// Repository for managing Role entities.
    /// </summary>
    public sealed class PostRepository : BaseRepository<Posts>, IPostRepository
    {
        /// <summary>
        /// Initializes a new instance of the PostRepository class.
        /// </summary>
        /// <param name="context">The application context.</param>
        public PostRepository(IApplicationContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Posts>> GetForPaginationAsync(int page, int quantity)
        {
            return await _entitySet.AsNoTracking().OrderByDescending(p => p.CreatedDate).ThenBy(p => p.Name).Skip((page - 1) * quantity).Take(quantity).Include(u => u.Author).ToListAsync();
        }

        public override async Task<IEnumerable<Posts>> GetAllAsync()
        {
            return await _entitySet.AsNoTracking().OrderByDescending(p => p.CreatedDate).ThenBy(p => p.Name).Include(u => u.Author).ToListAsync();
        }

        public override async Task<Posts?> GetByIdAsync(Guid id, bool noTracking = true)
        {
            return noTracking ? await _entitySet.AsNoTracking().Include(u => u.Author).FirstOrDefaultAsync(x => x.Id == id) :
                await _entitySet.Include(u => u.Author).FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
