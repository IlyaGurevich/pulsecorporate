﻿using Microsoft.EntityFrameworkCore;
using Npgsql;
using Post.API.Infrastructure.Interfaces;
using Post.API.Infrastructure.ServiceInfrastructure.Persistance;

namespace Post.API.Infrastructure.ServiceInfrastructure
{
    public static class ConfigureServices
    {
        public static IServiceCollection AddInfrastructureServices(this IServiceCollection services, IConfiguration configuration)
        {
            var portString = configuration["PostgresPort"];
            portString = string.IsNullOrEmpty(portString) ? "5432" : portString;
            int port = int.Parse(portString);

            var conStrBuilder = new NpgsqlConnectionStringBuilder(configuration.GetConnectionString("PostContext"))
            {
                Password = configuration["PostgresPassword"],
                Host = configuration["PostgresHost"],
                Port = port,
                Username = configuration["PostgresUsername"],
                Database = configuration["PostgresDatabase"]
            };

            var pulseCorporateContext = conStrBuilder.ConnectionString;
            services.AddDbContext<PostContext>(options => options.UseNpgsql(pulseCorporateContext
                , x => x.MigrationsAssembly("PulseCorporate.Infrastructure.PostgreSql")));

            services.AddTransient<IApplicationContext>(provider => provider.GetRequiredService<PostContext>());

            return services;
        }


        public static async void InitializeInfrastructureServices(this IServiceProvider provider)
        {
            using var scope = provider.CreateScope();
            var dbContext = scope.ServiceProvider.GetRequiredService<PostContext>();
            //dbContext.Database.EnsureDeleted();
            dbContext.Database.EnsureCreated();

            //await dbContext.Database.MigrateAsync();
        }
    }

}
