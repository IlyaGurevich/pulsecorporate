﻿using System.Reflection.Emit;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Post.API.Infrastructure.Interfaces;

namespace Post.API.Infrastructure.ServiceInfrastructure.Persistance
{
    public class PostContext : DbContext, IApplicationContext
    {
        public PostContext(DbContextOptions<PostContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            base.OnModelCreating(modelBuilder);
        }
    }
    
}
