﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Post.API.Domain.Entities;

namespace Post.API.Infrastructure.ServiceInfrastructure.Persistance.Configurations
{
    internal class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Name)
                .HasMaxLength(50);
            builder.Property(x => x.Surname)
                .HasMaxLength(50);

            builder.Property(x => x.Email)
                .HasMaxLength(255);
        }
    }
}
