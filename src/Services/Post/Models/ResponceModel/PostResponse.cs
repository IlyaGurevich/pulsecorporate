﻿namespace Post.API.Models.ResponceModel
{
    internal sealed record PostResponse(
         Guid Id,
         string Name,
         string Description,
         DateTime CreatedDate,
         UserResponseShort Author);
}
