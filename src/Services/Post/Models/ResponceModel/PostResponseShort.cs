﻿namespace Post.API.Models.ResponceModel
{
    internal sealed record PostResponseShort(
       Guid Id,
       string Name,
       string Description,
       DateTime CreatedDate,
       Guid AuthorId);
}
