﻿using Post.API.Domain.Abstractions;

namespace Post.API.Domain.Entities
{
    public class Posts : BaseEntity
    {
        public required string Name { get; set; }
        public string? Description { get; set; }
        public DateTime CreatedDate { get; set; }

        public required Guid AuthorId { get; set; }
        public required User Author { get; set; }
    }
}
