﻿namespace Post.API.Domain.Abstractions
{
    public abstract class BaseEntity
    {
        /// <summary>
        /// Gets or sets the unique identifier of the entity.
        /// </summary>
        public Guid Id { get; set; }
    }
}
