﻿using AutoMapper;
using Post.API.Domain.EntitiesDto;
using Post.API.Models;
using Post.API.Models.ResponceModel;

namespace Post.API.Mapping
{
    public sealed class UserUiProfile : Profile
    {
        public UserUiProfile()
        {
            CreateMap<UserDto, UserResponseShort>();
            CreateMap<UserModel, UserDto>()
                .ForMember(x => x.Id, map => map.Ignore())
                .ForMember(x => x.Email, map => map.Ignore());
        }
    }
}
