﻿using AutoMapper;
using Post.API.Domain.EntitiesDto;
using Post.API.Models;
using Post.API.Models.ResponceModel;

namespace Post.API.Mapping
{
    public class PostUiProfile : Profile
    {
        public PostUiProfile()
        {
            CreateMap<PostDto, PostResponse>();
            CreateMap<PostDto, PostResponseShort>();
            CreateMap<PostModel, PostDto>()
                .ForMember(x => x.Id, map => map.Ignore())
                .ForMember(x => x.Author, map => map.Ignore())
                .ForMember(x => x.AuthorId, map => map.Ignore());
        }
    }
}
