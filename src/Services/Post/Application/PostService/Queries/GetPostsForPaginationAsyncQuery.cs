﻿using MediatR;
using Post.API.Domain.EntitiesDto;

namespace Post.API.Application.PostService.Queries
{
    public record GetPostsForPaginationAsyncQuery(int Page, int Quantity) : IRequest<IEnumerable<PostDto>>;
}
