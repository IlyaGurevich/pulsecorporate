﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Caching.Distributed;
using Post.API.Application.Abstractions;
using Post.API.Application.PostService.Queries;
using Post.API.Domain.Entities;
using Post.API.Domain.EntitiesDto;
using System.Text.Json;

namespace Post.API.Application.PostService.QueriesHandler
{
    public class GetPostByIdHandler : IRequestHandler<GetPostByIdAsyncQuery, PostDto>
    {
        private readonly IPostRepository _postRepository;
        private readonly IMapper _mapper;
        private readonly IDistributedCache _distributedCache;

        public GetPostByIdHandler(IPostRepository postRepository, IMapper mapper, IDistributedCache distributedCache)
        {
            _postRepository = postRepository;
            _mapper = mapper;
            _distributedCache = distributedCache;
        }

        public async Task<PostDto> Handle(GetPostByIdAsyncQuery request, CancellationToken cancellationToken)
        {
            string serializedPost = await _distributedCache.GetStringAsync($"posts:{request.Id}");
            if (serializedPost != null)
            {
                var deserializedPost = JsonSerializer.Deserialize<Posts>(serializedPost);
                return _mapper.Map<PostDto>(deserializedPost);
            }

            var existPost = await _postRepository.GetByIdAsync(request.Id) ??
                throw new KeyNotFoundException($"Not found {nameof(Post)} with this id {request.Id}");

            if (existPost != null)
            {
                await _distributedCache.SetStringAsync(
                   key: $"posts:{request.Id}",
                   value: JsonSerializer.Serialize(value: existPost),
                   options: new DistributedCacheEntryOptions
                   {
                       SlidingExpiration = TimeSpan.FromSeconds(10),
                   });
            }

            return _mapper.Map<PostDto>(existPost);
        }
    }
}
