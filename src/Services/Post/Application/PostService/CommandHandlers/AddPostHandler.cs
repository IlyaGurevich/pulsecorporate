﻿using AutoMapper;
using MediatR;
using Post.API.Application.Abstractions;
using Post.API.Application.PostService.Commands;
using Post.API.Domain.Entities;
using Post.API.Domain.EntitiesDto;
using Post.API.RabbitMQ.Interfaces;

namespace Post.API.Application.PostService.CommandHandlers
{
    public class AddPostHandler : IRequestHandler<AddPostAsyncCommand, PostDto>
    {
        private readonly IUserRepository _userRepository;
        private readonly IPostRepository _postRepository;
        private readonly IMapper _mapper;
        private readonly IRabbitEmailProducer<EmailDto> _rabbit;

        public AddPostHandler(IPostRepository postRepository, IUserRepository userRepository, IMapper mapper, IRabbitEmailProducer<EmailDto> rabbit)
        {
            _postRepository = postRepository;
            _userRepository = userRepository;
            _mapper = mapper;
            _rabbit = rabbit;
        }

        public async Task<PostDto> Handle(AddPostAsyncCommand request, CancellationToken cancellationToken)
        {
            var email = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress")).Value;

            var existUser = await _userRepository.GetByEmailAsync(email);

            if (existUser is null)
            {
                var name = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname")).Value;
                var surName = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname")).Value;
                _userRepository.Add(new User { Name = name, Surname = surName, Email = email });
                await _userRepository.SaveChangesAsync(cancellationToken);

                existUser = await _userRepository.GetByEmailAsync(email);
            }

            request.Post.AuthorId = existUser!.Id;

            var newPost = _postRepository.Add(_mapper.Map<Posts>(request.Post));
            await _postRepository.SaveChangesAsync(cancellationToken);

            _rabbit.Send(new EmailDto(email, "Add post", $"Add post name - {request.Post.Name}, description - {request.Post.Description}"));

            newPost.Author = existUser;

            return _mapper.Map<PostDto>(newPost);
        }
    }
}
