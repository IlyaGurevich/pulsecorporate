﻿using AutoMapper;
using MediatR;
using Post.API.Application.Abstractions;
using Post.API.Application.PostService.Commands;
using Post.API.Domain.Entities;
using Post.API.Domain.EntitiesDto;
using Post.API.RabbitMQ.Interfaces;

namespace Post.API.Application.PostService.CommandHandlers
{
    public class UpdatePostHandler : IRequestHandler<UpdatePostAsyncCommand, PostDto>
    {
        private readonly IUserRepository _userRepository;
        private readonly IPostRepository _postRepository;
        private readonly IMapper _mapper;
        private readonly IRabbitEmailProducer<EmailDto> _rabbit;

        public UpdatePostHandler(IPostRepository postRepository, IUserRepository userRepository, IMapper mapper, IRabbitEmailProducer<EmailDto> rabbit)
        {
            _postRepository = postRepository;
            _userRepository = userRepository;
            _mapper = mapper;
            _rabbit = rabbit;
        }

        public async Task<PostDto> Handle(UpdatePostAsyncCommand request, CancellationToken cancellationToken)
        {
            var email = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress")).Value;

            var existUser = await _userRepository.GetByEmailAsync(email);

            if (existUser is null)
            {
                var name = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname")).Value;
                var surName = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname")).Value;
                _userRepository.Add(new User { Name = name, Surname = surName, Email = email });
                await _userRepository.SaveChangesAsync(cancellationToken);

                existUser = await _userRepository.GetByEmailAsync(email);
            }

            request.Post.AuthorId = existUser!.Id;
            request.Post.Author = _mapper.Map<UserDto>(existUser);
            request.Post.Id = request.Id;

            var existPost = await _postRepository.GetByIdAsync(request.Id) ??
               throw new KeyNotFoundException($"Not found {nameof(Posts)} with this id: {request.Id}");
            _mapper.Map(request.Post, existPost);

            _postRepository.Update(_mapper.Map<Posts>(existPost));
            await _postRepository.SaveChangesAsync(cancellationToken);

            _rabbit.Send(new EmailDto(email, "Update post", $"Update post new name - {request.Post.Name}, new description - {request.Post.Description}"));

            return request.Post;
        }
    }
}
