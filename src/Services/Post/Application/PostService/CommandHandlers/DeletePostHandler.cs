﻿using MediatR;
using Post.API.Application.Abstractions;
using Post.API.Application.PostService.Commands;
using Post.API.Domain.EntitiesDto;
using Post.API.RabbitMQ.Interfaces;

namespace Post.API.Application.PostService.CommandHandlers
{
    public class DeletePostHandler : IRequestHandler<DeletePostAsyncCommand, Guid>
    {
        private readonly IPostRepository _postRepository;
        private readonly IRabbitEmailProducer<EmailDto> _rabbit;

        public DeletePostHandler(IPostRepository postRepository, IRabbitEmailProducer<EmailDto> rabbit)
        {
            _postRepository = postRepository;
            _rabbit = rabbit;
        }

        public async Task<Guid> Handle(DeletePostAsyncCommand request, CancellationToken cancellationToken)
        {
            var email = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress")).Value;

            var existRole = await _postRepository.GetByIdAsync(request.Id) ??
                throw new KeyNotFoundException($"Not found {nameof(Post)} with this id: {request.Id}");

            _postRepository.Delete(existRole);
            await _postRepository.SaveChangesAsync(cancellationToken);

            _rabbit.Send(new EmailDto(email, "Delete post", $"Delete post id - {request.Id}"));

            return request.Id;
        }
    }
}
