﻿using MediatR;
using System.Security.Claims;

namespace Post.API.Application.PostService.Commands
{
    public record DeletePostAsyncCommand(Guid Id, IEnumerable<Claim> UserClaims) : IRequest<Guid>;
}
