﻿using MediatR;
using Post.API.Domain.EntitiesDto;
using System.Security.Claims;

namespace Post.API.Application.PostService.Commands
{
    public record AddPostAsyncCommand(PostDto Post, IEnumerable<Claim> UserClaims) : IRequest<PostDto>;
}
