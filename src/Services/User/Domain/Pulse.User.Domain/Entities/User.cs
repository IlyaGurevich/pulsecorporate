﻿using Pulse.User.Domain.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pulse.User.Domain.Entities
{
    public sealed class User : BaseEntity
    {
        public required string Name { get; set; }

        public required string Surname { get; set; }

        public required string Email { get; set; }

        //public ICollection<Meeting>? AddMeetings { get; set; }
        //public ICollection<Meeting>? Meetings { get; set; }
    }
}
