﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Npgsql;
using Pulse.User.Application.Interfaces;
using Pulse.User.Infrastructure.Persistence;

namespace Pulse.User.Infrastructure
{
    public static class ConfigureServices
    {
        public static IServiceCollection AddInfrastructureServices(this IServiceCollection services, IConfiguration configuration)
        {
            var portString = configuration["PostgresPort"];
            portString = string.IsNullOrEmpty(portString) ? "5432" : portString;
            int port = int.Parse(portString);

            var conStrBuilder = new NpgsqlConnectionStringBuilder(configuration.GetConnectionString("PulseUserContext"))
            {
                Password = configuration["PostgresPassword"],
                Host = configuration["PostgresHost"],
                Port = port,
                Username = configuration["PostgresUsername"],
                Database = configuration["PostgresDatabase"]
            };

            var pulseCorporateContext = conStrBuilder.ConnectionString;
            services.AddDbContext<PulseUserContext>(options => options.UseNpgsql(pulseCorporateContext
                , x => x.MigrationsAssembly("Pulse.User.PostgreSql")));

            services.AddTransient<IApplicationContext>(provider => provider.GetRequiredService<PulseUserContext>());

            return services;
        }


        public static void InitializeInfrastructureServices(this IServiceProvider provider)
        {
            using var scope = provider.CreateScope();
            var dbContext = scope.ServiceProvider.GetRequiredService<PulseUserContext>();            
            dbContext.Database.EnsureCreated();
        }
    }
}