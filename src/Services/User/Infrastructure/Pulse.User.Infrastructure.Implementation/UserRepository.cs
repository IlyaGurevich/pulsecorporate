﻿using Pulse.User.Application.Interfaces;
using Pulse.User.Infrastructure.Implementation.PulseCorporate.Repositories.Implementation;
using Pulse.User.Repositories.Abstractions.Interfaces;
using Pulse.User.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Pulse.User.Infrastructure.Implementation
{
    /// <summary>
    /// Repository for managing Role entities.
    /// </summary>
    public sealed class UserRepository : BaseRepository<Domain.Entities.User>, IUserRepository
    {

        /// <summary>
        /// Initializes a new instance of the UserRepository class.
        /// </summary>
        /// <param name="context">The application context.</param>
        public UserRepository(IApplicationContext context) : base(context)
        {
        }

        public async Task<Domain.Entities.User?> GetByEmailAsync(string email, bool noTracking = true)
        {
            return noTracking ? await _entitySet.AsNoTracking().FirstOrDefaultAsync(x => string.Equals(x.Email, email)) :
                await _entitySet.FirstOrDefaultAsync(x => string.Equals(x.Email, email));
        }

        public async Task<ICollection<Domain.Entities.User>> GetUsersById(IEnumerable<Guid> ids, bool noTracking = true)
        {
            return noTracking ? await _entitySet.AsNoTracking().Where(u => ids.Contains(u.Id)).AsNoTracking().ToListAsync() :
                await _entitySet.Where(u => ids.Contains(u.Id)).ToListAsync();
        }
    }
}
