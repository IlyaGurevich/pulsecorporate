﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pulse.User.Domain.Entities;

namespace Pulse.User.Repositories.Abstractions.Interfaces
{
    public interface IUserRepository : IRepository<Domain.Entities.User>
    {
        Task<Domain.Entities.User?> GetByEmailAsync(string email, bool noTracking = true);

        Task<ICollection<Domain.Entities.User>> GetUsersById(IEnumerable<Guid> ids, bool noTracking = true);
    }
}
