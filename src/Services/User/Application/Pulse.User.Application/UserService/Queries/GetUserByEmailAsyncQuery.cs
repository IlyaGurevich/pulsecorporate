﻿using MediatR;
using Pulse.User.Domain.EntitiesDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Pulse.User.Application.UserService.Queries
{
    public record GetUserByEmailAsyncQuery(IEnumerable<Claim> UserClaims) : IRequest<UserDto>;
}
