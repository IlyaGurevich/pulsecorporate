﻿using AutoMapper;
using Calendar.API.Domain.Entities;
using Calendar.API.Domain.EntitiesDto;

namespace Calendar.API.Application.Mapping
{
    public sealed class MeetingProfile : Profile
    {
        public MeetingProfile()
        {
            CreateMap<MeetingDto, Meeting>()
                .ForAllMembers(opt => opt.Condition((src, dest, srcMember) => srcMember is not null));
            CreateMap<Meeting, MeetingDto>()
                .ForMember(x => x.ParticipantsId, map => map.Ignore());
        }
    }
}
