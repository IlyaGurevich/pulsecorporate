﻿using Calendar.API.Application.Abstractions;
using Calendar.API.Application.MeetingService.Commands;
using Calendar.API.Domain.Entities;
using Calendar.API.Domain.EntitiesDto;
using Calendar.API.RabbitMQ.Interfaces;
using MediatR;

namespace Calendar.API.Application.CalendarService.CommandHandlers
{
    public class DeleteMeetingHandler : IRequestHandler<DeleteMeetingAsyncCommand, Guid>
    {
        private readonly IMeetingRepository _meetingRepository;
        private readonly IRabbitEmailProducer<EmailDto> _rabbit;

        public DeleteMeetingHandler(IMeetingRepository meetingRepository, IRabbitEmailProducer<EmailDto> rabbit)
        {
            _meetingRepository = meetingRepository;
            _rabbit = rabbit;
        }

        public async Task<Guid> Handle(DeleteMeetingAsyncCommand request, CancellationToken cancellationToken)
        {

            var email = request. UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress")).Value;

            var existMeeting = await _meetingRepository.GetByIdAsync(request.Id) ??
                throw new KeyNotFoundException($"Not found {nameof(Meeting)} with this id: {request.Id}");

            _meetingRepository.Delete(existMeeting);
            await _meetingRepository.SaveChangesAsync(cancellationToken);

            _rabbit.Send(new EmailDto(email, "Delete meeting", $"Delete meeting id - {request.Id}"));

            return request.Id;
        }
    }
}
