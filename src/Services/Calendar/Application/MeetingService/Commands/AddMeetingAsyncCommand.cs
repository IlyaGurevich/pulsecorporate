﻿using Calendar.API.Domain.EntitiesDto;
using MediatR;
using System.Security.Claims;

namespace Calendar.API.Application.MeetingService.Commands
{
    public record AddMeetingAsyncCommand(MeetingDto Meeting, IEnumerable<Claim> UserClaims) : IRequest<MeetingDto>;
}
