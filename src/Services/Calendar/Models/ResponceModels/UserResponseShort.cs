﻿namespace Calendar.API.Models.ResponceModels
{
    internal sealed record UserResponseShort(
         Guid Id,
         string Name,
         string Surname,
         string Email);
}
