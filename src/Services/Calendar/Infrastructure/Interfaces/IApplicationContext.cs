﻿using Microsoft.EntityFrameworkCore;

namespace Calendar.API.Infrastructure.Interfaces
{
    public interface IApplicationContext
    {
        DbSet<T> Set<T>() where T : class;

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
