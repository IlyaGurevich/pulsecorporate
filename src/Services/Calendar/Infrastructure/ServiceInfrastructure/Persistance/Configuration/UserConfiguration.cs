﻿using Calendar.API.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Calendar.API.Infrastructure.ServiceInfrastructure.Persistance.Configuration
{
    public class UserConfiguration: IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Name)
                .HasMaxLength(50);
            builder.Property(x => x.Surname)
                .HasMaxLength(50);

            builder.Property(x => x.Email)
                .HasMaxLength(255);
        }
    }
}
