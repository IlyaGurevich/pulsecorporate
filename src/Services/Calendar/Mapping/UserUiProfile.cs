﻿using AutoMapper;
using Calendar.API.Domain.EntitiesDto;
using Calendar.API.Models;
using Calendar.API.Models.ResponceModels;

namespace Calendar.API.Mapping
{
    public sealed class UserUiProfile : Profile
    {
        public UserUiProfile()
        {
            CreateMap<UserDto, UserResponseShort>();
            CreateMap<UserModel, UserDto>()
                .ForMember(x => x.Id, map => map.Ignore())
                .ForMember(x => x.Email, map => map.Ignore());
        }
    }
}
