﻿using AutoMapper;
using Calendar.API.Domain.EntitiesDto;
using Calendar.API.Models;
using Calendar.API.Models.ResponceModels;

namespace Calendar.API.Mapping
{
    public class MeetingUiProfile : Profile
    {
        public MeetingUiProfile()
        {
            CreateMap<MeetingDto, MeetingResponse>();
            CreateMap<MeetingModel, MeetingDto>()
                .ForMember(x => x.Id, map => map.Ignore())
                .ForMember(x => x.CreatorId, map => map.Ignore())
                .ForMember(x => x.Creator, map => map.Ignore())
                .ForMember(x => x.Participants, map => map.Ignore());
        }
    }
}
