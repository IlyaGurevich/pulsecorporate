﻿using PulseCorporate.Domain.Abstractions;
using PulseCorporate.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Domain.EntitiesDto
{
    /// <summary>
    /// Data Transfer Object (DTO) for representing a user.
    /// </summary>
    public sealed class UserDto : BaseEntityDto
    {
        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        public required string Name { get; set; }

        /// <summary>
        /// Gets or sets the surname of the user.
        /// </summary>      
        public required string Surname { get; set; }

        /// <summary>
        /// Gets or sets the email of the user.
        /// </summary> 
        public required string Email { get; set; }
    }
}
