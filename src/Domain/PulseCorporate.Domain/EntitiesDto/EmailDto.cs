﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Domain.EntitiesDto
{
    public class EmailDto
    {
        public EmailDto(string email, string subject, string message)
        {
            Email = email;
            Subject = subject;
            Message = message;
        }
        public string Email { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
    }
}
