﻿using PulseCorporate.Domain.Abstractions;
using PulseCorporate.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Domain.EntitiesDto
{
    public sealed class PostDto : BaseEntityDto
    {
        /// <summary>
        /// Gets or sets the name of the post.
        /// </summary>
        public required string Name { get; set; }

        /// <summary>
        /// Gets or sets the description of the post.
        /// </summary>
        public string? Description { get; set; }

        /// <summary>
        /// Gets or sets the collection of users associated with this post.
        /// </summary>
        //public required User Author { get; set; }

        public DateTime? CreatedDate { get; set; }

        public required Guid AuthorId { get; set; }
        public required UserDto Author { get; set; }
    }
}
