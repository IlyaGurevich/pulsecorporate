﻿using Microsoft.EntityFrameworkCore;
using PulseCorporate.Application.Interfaces;
using PulseCorporate.Domain.Entities;
using PulseCorporate.Repositories.Abstractions.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Repositories.Implementation.Administration
{
    /// <summary>
    /// Repository for managing Role entities.
    /// </summary>
    public sealed class PostRepository : BaseRepository<Post>, IPostRepository
    {
        /// <summary>
        /// Initializes a new instance of the PostRepository class.
        /// </summary>
        /// <param name="context">The application context.</param>
        public PostRepository(IApplicationContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Post>> GetForPaginationAsync(int page, int quantity)
        {
            return await _entitySet.AsNoTracking().OrderByDescending(p => p.CreatedDate).ThenBy(p => p.Name).Skip((page - 1) * quantity).Take(quantity).Include(u => u.Author).ToListAsync();
        }

        public override async Task<IEnumerable<Post>> GetAllAsync()
        {
            return await _entitySet.AsNoTracking().OrderByDescending(p => p.CreatedDate).ThenBy(p => p.Name).Include(u=>u.Author).ToListAsync();
        }

        public override async Task<Post?> GetByIdAsync(Guid id, bool noTracking = true)
        {
            return noTracking ? await _entitySet.AsNoTracking().Include(u=>u.Author).FirstOrDefaultAsync(x => x.Id == id) :
                await _entitySet.Include(u => u.Author).FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
