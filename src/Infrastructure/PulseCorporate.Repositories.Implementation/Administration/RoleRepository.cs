﻿using Microsoft.EntityFrameworkCore;
using PulseCorporate.Application.Interfaces;
using PulseCorporate.Domain.Entities;
using PulseCorporate.Repositories.Abstractions.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Repositories.Implementation.Administration
{
    /// <summary>
    /// Repository for managing Role entities.
    /// </summary>
    public sealed class RoleRepository : BaseRepository<Role>, IRoleRepository
    {

        /// <summary>
        /// Initializes a new instance of the RoleRepository class.
        /// </summary>
        /// <param name="context">The application context.</param>
        public RoleRepository(IApplicationContext context) : base(context)
        {
        }        
    }
}
