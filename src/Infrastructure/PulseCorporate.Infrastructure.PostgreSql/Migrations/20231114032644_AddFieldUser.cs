﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace PulseCorporate.Infrastructure.PostgreSql.Migrations
{
    /// <inheritdoc />
    public partial class AddFieldUser : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Post",
                keyColumn: "Id",
                keyValue: new Guid("90189aa0-3086-4d5b-8283-79dc90b91c95"));

            migrationBuilder.DeleteData(
                table: "Post",
                keyColumn: "Id",
                keyValue: new Guid("bf89b965-3efb-40f5-ace3-895f86aaf1c7"));

            migrationBuilder.DeleteData(
                table: "Post",
                keyColumn: "Id",
                keyValue: new Guid("c7a490b3-b458-4560-9c15-ca557a234cdb"));

            migrationBuilder.DeleteData(
                table: "Post",
                keyColumn: "Id",
                keyValue: new Guid("e9e9c58e-c7b0-4c9b-bd1d-845b318032a9"));

            migrationBuilder.AddColumn<Guid>(
                name: "AuthorId",
                table: "Post",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Post_AuthorId",
                table: "Post",
                column: "AuthorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Post_User_AuthorId",
                table: "Post",
                column: "AuthorId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Post_User_AuthorId",
                table: "Post");

            migrationBuilder.DropIndex(
                name: "IX_Post_AuthorId",
                table: "Post");

            migrationBuilder.DropColumn(
                name: "AuthorId",
                table: "Post");

            migrationBuilder.InsertData(
                table: "Post",
                columns: new[] { "Id", "CreatedDate", "Description", "Name" },
                values: new object[,]
                {
                    { new Guid("90189aa0-3086-4d5b-8283-79dc90b91c95"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "My family", "Post4" },
                    { new Guid("bf89b965-3efb-40f5-ace3-895f86aaf1c7"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "MyPuss", "Post1" },
                    { new Guid("c7a490b3-b458-4560-9c15-ca557a234cdb"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Elbrus", "Post2" },
                    { new Guid("e9e9c58e-c7b0-4c9b-bd1d-845b318032a9"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Me", "Post3" }
                });
        }
    }
}
