﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Npgsql;
using PulseCorporate.Application.Interfaces;
using PulseCorporate.Infrastructure.Persistence;

namespace PulseCorporate.Infrastructure;
public static class ConfigureServices
{
    public static IServiceCollection AddInfrastructureServices(this IServiceCollection services, IConfiguration configuration)
    {
        var portString = configuration["PostgresPort"];
        portString = string.IsNullOrEmpty(portString) ? "5432" : portString;
        int port = int.Parse(portString);

        var conStrBuilder = new NpgsqlConnectionStringBuilder(configuration.GetConnectionString("PulseCorporateContext"))
        {
            Password = configuration["PostgresPassword"],
            Host = configuration["PostgresHost"],
            Port = port,
            Username = configuration["PostgresUsername"],
            Database = configuration["PostgresDatabase"]
        };

        var pulseCorporateContext = conStrBuilder.ConnectionString;
        services.AddDbContext<PulseCorporateContext>(options => options.UseNpgsql(pulseCorporateContext
            , x => x.MigrationsAssembly("PulseCorporate.Infrastructure.PostgreSql")));

        services.AddTransient<IApplicationContext>(provider => provider.GetRequiredService<PulseCorporateContext>());

        return services;
    }


    public static async void InitializeInfrastructureServices(this IServiceProvider provider)
    {
        using var scope = provider.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<PulseCorporateContext>();
        //dbContext.Database.EnsureDeleted();
        dbContext.Database.EnsureCreated();

        //await dbContext.Database.MigrateAsync();
    }
}
