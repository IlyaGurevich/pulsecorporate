﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PulseCorporate.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Infrastructure.Persistence.Configurations
{
    internal class MeetingConfiguration : IEntityTypeConfiguration<Meeting>
    {
        public void Configure(EntityTypeBuilder<Meeting> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(x => x.Topic)
                .HasMaxLength(50);
            builder.Property(x => x.Description)
                .HasMaxLength(250);
            builder.HasMany(c => c.Participants)
                .WithMany(s => s.Meetings)
                .UsingEntity(j => j.ToTable("UserMeetings"));
            builder.HasOne(m => m.Creator)
                .WithMany(u => u.AddMeetings)
                .HasForeignKey(m => m.CreatorId);
        }
    }
}
