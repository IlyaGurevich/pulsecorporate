﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using PulseCorporate.Domain.Entities;

namespace PulseCorporate.Infrastructure.Persistence.Configurations;
internal class UserConfiguration : IEntityTypeConfiguration<User>
{
    public void Configure(EntityTypeBuilder<User> builder)
    {
        builder.HasKey(x => x.Id);

        builder.Property(x => x.Name)
            .HasMaxLength(50);
        builder.Property(x => x.Surname)
            .HasMaxLength(50);

        builder.Property(x => x.Email)
            .HasMaxLength(255);        
    }
}
