﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PulseCorporate.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Infrastructure.Persistence.Configurations
{
    internal class PostConfiguration:IEntityTypeConfiguration<Post>
    {
        public void Configure(EntityTypeBuilder<Post> builder) 
        { 
            builder.HasKey(e => e.Id);
            builder.Property(x=>x.Name)
                .HasMaxLength(50);
        }
    }
}
