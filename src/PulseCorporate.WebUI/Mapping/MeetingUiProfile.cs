﻿using AutoMapper;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.WebUI.Models.Administration;
using PulseCorporate.WebUI.ResponseModels;
using PulseCorporate.WebUI.ResponseModels.Administration;

namespace PulseCorporate.WebUI.Mapping
{
    public class MeetingUiProfile : Profile
    {
        public MeetingUiProfile()
        {
            CreateMap<MeetingDto, MeetingResponse>();
            CreateMap<MeetingModel, MeetingDto>()
                .ForMember(x => x.Id, map => map.Ignore())
                .ForMember(x => x.CreatorId, map => map.Ignore())
                .ForMember(x => x.Creator, map => map.Ignore())
                .ForMember(x => x.Participants, map => map.Ignore());
        }
    }
}
