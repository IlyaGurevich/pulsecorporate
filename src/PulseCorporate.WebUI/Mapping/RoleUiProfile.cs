﻿using AutoMapper;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.WebUI.Models.Administration;
using PulseCorporate.WebUI.ResponseModels.Administration;

namespace PulseCorporate.WebUI.Mapping
{
    public sealed class RoleUiProfile : Profile
    {
        public RoleUiProfile()
        {
            CreateMap<RoleDto, RoleResponse>();
            CreateMap<RoleDto, RoleResponseShort>();
            CreateMap<RoleModel, RoleDto>()
                .ForMember(x => x.Id, map => map.Ignore());
        }
    }
}
