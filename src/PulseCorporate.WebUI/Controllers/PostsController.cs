﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using PulseCorporate.Application.PostService.Commands;
using PulseCorporate.Application.PostService.Queries;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.WebUI.Models.Administration;
using PulseCorporate.WebUI.ResponseModels.Administration;

namespace PulseCorporate.WebUI.Controllers
{
    /// <summary>
    /// API controller for managing posts.
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class PostsController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ISender _sender;
        private readonly ILogger<PostsController> _logger;

        /// <summary>
        /// Initializes a new instance of the PostsController class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="mapper">The mapper.</param>
        /// <param name="sender">The sender for MediatR requests.</param>
        public PostsController(ILogger<PostsController> logger, IMapper mapper, ISender sender)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger), "Uninitialized property");
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper), "Uninitialized property");
            _sender = sender ?? throw new ArgumentNullException(nameof(sender), "Uninitialized property");
        }


        /// <summary>
        /// Retrieves a list of posts.
        /// </summary>
        /// <returns>A list of posts.</returns>
        [HttpGet]
        [ProducesResponseType(typeof(List<PostResponse>), 200)]
        [ProducesResponseType(typeof(List<PostResponseShort>), 200)]
        public async Task<IActionResult> GetPosts([FromQuery] int page, [FromQuery] int limit)
        {
            if (page > 0 && limit > 0)
            {
                return Ok(_mapper.Map<List<PostResponse>>(await _sender.Send(new GetPostsForPaginationAsyncQuery(page, limit))));
            }

            return Ok(_mapper.Map<List<PostResponseShort>>(await _sender.Send(new GetPostsAsyncQuery())));
        }

        /// <summary>
        /// Retrieves a post by its unique identifier.
        /// </summary>
        /// <param name="id">The unique identifier of the Post.</param>
        /// <returns>The post with the specified identifier.</returns>
        [HttpGet("{id:Guid}", Name = "GetPostById")]
        [ProducesResponseType(typeof(PostResponseShort), 200)]
        public async Task<ActionResult> GetPostById(Guid id)
        {
            var post = _mapper.Map<PostResponse>(await _sender.Send(new GetPostByIdAsyncQuery(id)));

            return Ok(post);
        }

        /// <summary>
        /// Adds a new post.
        /// </summary>
        /// <param name="postModel">The post details to add.</param>
        /// <returns>The newly added post.</returns>
        [HttpPost]
        [ProducesResponseType(typeof(PostResponse), 201)]
        public async Task<ActionResult> AddPost([FromBody] PostModel postModel)
        {
            var addedPost = await _sender.Send(new AddPostAsyncCommand(_mapper.Map<PostDto>(postModel), HttpContext.User.Claims));

            return CreatedAtRoute("GetPostById", new { id = addedPost.Id }, _mapper.Map<PostResponse>(addedPost));
        }

        /// <summary>
        /// Updates an existing post.
        /// </summary>
        /// <param name="id">The unique identifier of the post to update.</param>
        /// <param name="postModel">The updated post details.</param>
        /// <returns>The updated post.</returns>
        [HttpPut("{id:Guid}")]
        [ProducesResponseType(typeof(PostResponse), 202)]
        public async Task<ActionResult> UpdatePost(Guid id, [FromBody] PostModel postModel)
        {
            var updatePost = await _sender.Send(new UpdatePostAsyncCommand(id, _mapper.Map<PostDto>(postModel), HttpContext.User.Claims));

            return AcceptedAtRoute("GetPostById", new { id = updatePost.Id }, _mapper.Map<PostResponse>(updatePost));
        }

        /// <summary>
        /// Deletes a post by its unique identifier.
        /// </summary>
        /// <param name="id">The unique identifier of the post to delete.</param>
        /// <returns>A message confirming the deletion.</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(string), 200)]
        public async Task<IActionResult> DeletePost(Guid id)
        {
            await _sender.Send(new DeletePostAsyncCommand(id, HttpContext.User.Claims));

            return Ok($"Post with id = {id} has been removed");
        }
    }
}
