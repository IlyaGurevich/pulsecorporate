﻿namespace PulseCorporate.WebUI.ResponseModels.Administration
{
    internal sealed record PostResponseShort(
        Guid Id,
        string Name,
        string Description,
        DateTime CreatedDate,
        Guid AuthorId);
    
}
