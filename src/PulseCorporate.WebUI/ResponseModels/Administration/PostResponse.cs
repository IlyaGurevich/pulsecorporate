﻿namespace PulseCorporate.WebUI.ResponseModels.Administration
{
    internal sealed record PostResponse(
        Guid Id,
        string Name,
        string Description,
        DateTime CreatedDate,
        UserResponseShort Author);
}
