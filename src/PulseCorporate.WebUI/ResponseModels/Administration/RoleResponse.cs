﻿using PulseCorporate.Domain.Entities;

namespace PulseCorporate.WebUI.ResponseModels.Administration
{
    internal sealed record RoleResponse(
        Guid Id,
        string Name,
        string Description);
}
