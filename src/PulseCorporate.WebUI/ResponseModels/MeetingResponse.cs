﻿using PulseCorporate.Domain.Entities;
using PulseCorporate.WebUI.ResponseModels.Administration;

namespace PulseCorporate.WebUI.ResponseModels
{
    internal sealed record MeetingResponse(
        Guid Id,
        string Topic,
        string Description,
        DateTime StartTime,
        DateTime EndTime,
        UserResponseShort Creator,
        IEnumerable<UserResponseShort> Participants);    
}
