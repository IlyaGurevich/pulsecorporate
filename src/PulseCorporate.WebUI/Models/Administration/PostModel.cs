﻿namespace PulseCorporate.WebUI.Models.Administration
{
    public sealed class PostModel
    {
        /// <summary>
        /// Gets or sets the name of the post.
        /// </summary>
        public required string Name { get; set; }

        /// <summary>
        /// Gets or sets the description of the post.
        /// </summary>
        public string? Description { get; set; }

        public DateTime? CreatedDate { get; set; }
    }
}
