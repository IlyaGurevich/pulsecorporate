﻿
namespace EmailNotification.DAL.Interfaces
{
    internal interface IRepository<T>
    {
        Task AddToMongo(T item);
    }
}
