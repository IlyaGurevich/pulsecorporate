﻿using Microsoft.Extensions.Configuration;
using Serilog.Sinks.Elasticsearch;
using Serilog;
using System.Reflection;
using Serilog.Exceptions;
using MongoDB.Driver;
using MongoDB.Bson;
using EmailNotification.DAL.Repository;
using EmailNotification.DAL.Interfaces;
using EmailNotification.DAL.EntityDto;
using EmailNotification.BL.Service;

namespace EmailNotification
{
    internal static class Registrar
    {
        public static List<Func<EmailDto, Task>> ConfigureServices()
        {
            var mongoRepository = ConfigureMongoDb();
            EmailService emailService = new EmailService();
            List<Func<EmailDto, Task>> services = new List<Func<EmailDto, Task>>()
            {
                emailService.SendEmailAsync,
                mongoRepository.AddToMongo
            };
            return services;
        }

        public static void ConfigureLogging()
        {
            string environment = "Development";

            var configuration =BuildConfiguration();
            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .Enrich.WithExceptionDetails()
                .WriteTo.Debug()
                .WriteTo.Console()
                .WriteTo.Elasticsearch(ConfigureElasticSink(configuration, environment))
                .Enrich.WithProperty("Environment", environment)
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
        }

        public static IRepository<EmailDto> ConfigureMongoDb()
        {
            var configuration = BuildConfiguration();
            MongoClient client = new MongoClient(configuration.GetConnectionString("Mongo"));
            IMongoDatabase database = client.GetDatabase(configuration["MongoDataBaseName"]);
            var collection = database.GetCollection<BsonDocument>(configuration["MongoCollectionName"]);
            MongoRepository<EmailDto> mongoRepository = new MongoRepository<EmailDto>(collection);
            return mongoRepository;
        }

        private static ElasticsearchSinkOptions ConfigureElasticSink(IConfigurationRoot configuration, string environment)
        {
            return new ElasticsearchSinkOptions(new Uri(configuration["ElastcConfiguration:Uri"]!))
            {
                AutoRegisterTemplate = true,
                IndexFormat = $"{Assembly.GetExecutingAssembly().GetName().Name!.ToLower().Replace(".", "-")}-{environment.ToLower()}-{DateTime.UtcNow:yyyy-MM-dd}",
                NumberOfReplicas = 1,
                NumberOfShards = 2
            };
        }
        private static IConfigurationRoot BuildConfiguration()
        {
            string environment = "Development";

            return new ConfigurationBuilder()
                .AddJsonFile("mailsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"mailsettings.{environment}.json", optional: true)
                .Build();
        }
    }
}
