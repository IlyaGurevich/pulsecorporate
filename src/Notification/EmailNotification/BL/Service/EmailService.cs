﻿using EmailNotification.DAL.EntityDto;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Configuration;
using MimeKit;
using Serilog;

namespace EmailNotification.BL.Service
{
    public class EmailService
    {
        IConfiguration _configuration;

        public EmailService()
        {
            _configuration = new ConfigurationBuilder()
                .AddJsonFile("mailsettings.json", optional: true, reloadOnChange: true)
                .Build();
        }

        public async Task SendEmailAsync(EmailDto emailDto)
        {

            Log.Information("Sending email {@EmailDto}", emailDto);

            using var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress(_configuration["MailRu:FromName"], _configuration["MailRu:FromMail"]));
            emailMessage.To.Add(new MailboxAddress("", emailDto.Email));
            emailMessage.Subject = emailDto.Subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = emailDto.Message
            };

            using var client = new SmtpClient();
            int.TryParse(_configuration["MailRu:Port"], out int emailPort);
            bool.TryParse(_configuration["MailRu:IsSSL"], out bool IsSSL);
            await client.ConnectAsync(_configuration["MailRu:SMTP"], emailPort, IsSSL);
            await client.AuthenticateAsync(_configuration["MailRu:FromMail"], _configuration["MailRu:Password"]);
            await client.SendAsync(emailMessage);
            await client.DisconnectAsync(true);
        }


    }
}
