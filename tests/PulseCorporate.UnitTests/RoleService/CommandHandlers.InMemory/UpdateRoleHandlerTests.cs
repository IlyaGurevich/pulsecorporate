﻿using AutoFixture;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using PulseCorporate.Application.RoleService.CommandHandlers;
using PulseCorporate.Application.RoleService.Commands;
using PulseCorporate.Domain.Entities;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.Repositories.Abstractions.Administration;
using Xunit;

namespace tests.PulseCorporate.UnitTests.RoleService.CommandHandlers.InMemory
{
    public class UpdateRoleHandlerTests: IClassFixture<RoleFixtureInMemory>
    {
        IRoleRepository _roleRepository;
        UpdateRoleHandler _handle;
        IMapper _mapper;
        Fixture _autoFixture = new Fixture();
        public UpdateRoleHandlerTests(RoleFixtureInMemory roleFixtureInMemory) 
        {
            var serviceProvider = roleFixtureInMemory.serviceProvider;
            _roleRepository = serviceProvider.GetService<IRoleRepository>();
            _mapper = serviceProvider.GetService<IMapper>();
            _handle = new UpdateRoleHandler(_roleRepository, _mapper);
        }

        [Fact]
        public async void Handle_Returns_Collection_With_Updated_Element()
        {
            //Arrange
            var oldRole = _autoFixture.Build<Role>()
                .With(t => t.Id, new Guid("b07fad35-4a58-476e-adce-1a5a4e678b51"))
                .With(t => t.Name, "Admen")
                .Create();
            _roleRepository.Add(oldRole);
            await _roleRepository.SaveChangesAsync();

            var newRoleDto = _autoFixture.Build<RoleDto>()
                            .With(t => t.Id, new Guid("b07fad35-4a58-476e-adce-1a5a4e678b51"))
                            .With(t => t.Name, "Admin")
                            .Create();

            //Act
            var newRole = await _handle.Handle(new UpdateRoleAsyncCommand(oldRole.Id, newRoleDto), new CancellationToken());
            var roles = await _roleRepository.GetAllAsync();
            //Assert
            var updatedRole = await _roleRepository.GetByIdAsync(newRoleDto.Id);
            Assert.Equal(newRoleDto.Id, updatedRole.Id);
            Assert.Equal(newRoleDto.Name, updatedRole.Name);
        }

        [Fact]
        public async void Handle_Returns_KeyNotFoundException_If_Unknown_Id()
        {
            //Arrange
            var unknownGuid = new Guid("b07fad35-4a58-476e-adce-1a5a4e678b51");
            var roleDto = _autoFixture.Build<RoleDto>()
                .With(t => t.Id, new Guid("b07fad35-4a58-476e-adce-1a5a4e678b51"))
                .With(t => t.Name, "Admen")
                .Create();

            //Act
            var exception = await Assert.ThrowsAsync<KeyNotFoundException>(async () =>
            await _handle.Handle(new UpdateRoleAsyncCommand(unknownGuid, roleDto), new CancellationToken()));

            //Assert
            Assert.Equal($"Not found {nameof(Role)} with this id: {unknownGuid}", exception.Message);
        }
    }
}
