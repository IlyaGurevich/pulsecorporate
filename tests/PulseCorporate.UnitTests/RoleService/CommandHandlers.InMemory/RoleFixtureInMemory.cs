﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace tests.PulseCorporate.UnitTests.RoleService.CommandHandlers.InMemory
{
    public class RoleFixtureInMemory
    {
        public IServiceProvider serviceProvider;

        public RoleFixtureInMemory() 
        {
            var builder = new ConfigurationBuilder();
            var configuration = builder.Build();
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddServicesTests_InMemory(configuration);
            serviceProvider = serviceCollection.BuildServiceProvider();
        }
    }
}
