﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using PulseCorporate.Application.RoleService.Queries;
using PulseCorporate.Application.RoleService.QueriesHandlers;
using PulseCorporate.Domain.Entities;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.Repositories.Abstractions.Administration;
using tests.PulseCorporate.UnitTests.RoleService.QueriesHandlers.Mock;
using Xunit;

namespace tests.PulseCorporate.UnitTests.RoleService.QueriesHandlers.Moq
{
    public class GetRoleByIdHandlerTests: IClassFixture<RoleFixture>
    {
        private readonly Mock<IRoleRepository> _roleRepository;
        private GetRoleByIdHandler _handler;

        public GetRoleByIdHandlerTests(RoleFixture roleFixture)
        {
            _roleRepository = new Mock<IRoleRepository>();

            var serviceProvider = roleFixture.ServiceProvider;
            _roleRepository.Setup(r => r.GetByIdAsync(new Guid("b73b6bd3-e516-4afd-9d68-be8f5180bd62"),false)).ReturnsAsync(
                new Role
                {
                    Id = new Guid("b73b6bd3-e516-4afd-9d68-be8f5180bd62"),
                    Name = "User",
                    Description = "I need help"
                });
            _handler = new GetRoleByIdHandler(_roleRepository.Object, serviceProvider.GetService<IMapper>());
        }
        [Fact]
        public void Handle_Returns_RoleDtoById_For_Valid_Guid()
        {
            //Arrange
            RoleDto compareResult = new RoleDto
            {
                Id = new Guid("b73b6bd3-e516-4afd-9d68-be8f5180bd62"),
                Name = "User",
                Description = "I need help"
            };

            //Act
            var role = _handler.Handle(new GetRoleByIdAsyncQuery(new Guid("b73b6bd3-e516-4afd-9d68-be8f5180bd62")), new CancellationToken());
            //Assert
            Assert.Equal(compareResult.Id, role.Result.Id);
            Assert.Equal(compareResult.Name, role.Result.Name);
            Assert.Equal(compareResult.Description, role.Result.Description);
        }

        [Fact]
        public async void Handle_Returns_KeyNotFoundException_For_Non_Valid_Guid()
        {
            //Arrange
            var nonExistingGuid = new Guid("a73b6bd3-e516-4afd-9d68-be8f5180bd62");

            //Act
            var exception = await Assert.ThrowsAsync<KeyNotFoundException>(() => _handler.Handle(new GetRoleByIdAsyncQuery(nonExistingGuid), new CancellationToken()));
            

            //Assert
            Assert.Equal($"Not found {nameof(Role)} with this id: {nonExistingGuid}", exception.Message);
           
        }
    }
}
