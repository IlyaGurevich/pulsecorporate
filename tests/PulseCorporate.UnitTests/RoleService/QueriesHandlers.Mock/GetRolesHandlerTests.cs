﻿
using AutoMapper;
using Moq;
using PulseCorporate.Repositories.Abstractions.Administration;
using Xunit;
using PulseCorporate.Application.RoleService.Queries;
using PulseCorporate.Application.RoleService.QueriesHandlers;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.Domain.Entities;
using tests.PulseCorporate.UnitTests.RoleService.QueriesHandlers.Mock;
using Microsoft.Extensions.DependencyInjection;
using AutoFixture;

namespace tests.PulseCorporate.UnitTests.RoleService.QueriesHandlers
{
    public class GetRolesHandlerTests:IClassFixture<RoleFixture>
    {
        private readonly Mock<IRoleRepository> _roleRepository;
        private GetRolesHandler _handler;

        public GetRolesHandlerTests(RoleFixture roleFixture)
        {
            var serviceProvider = roleFixture.ServiceProvider;

            _roleRepository = new Mock<IRoleRepository>();

            Fixture autoFixture = new Fixture();

            _roleRepository.Setup(r => r.GetAllAsync()).ReturnsAsync(new List<Role> {
                new Role { 
                    Id = new Guid("2ec1af40-5a2d-4e1d-8739-b9b60752482d"),
                    Name = "Admin", 
                    Description = "I'm admin. I need your MAC" }, 
                new Role { 
                    Id = new Guid("b73b6bd3-e516-4afd-9d68-be8f5180bd62"),
                    Name = "User", 
                    Description = "I need help" } });
            _handler = new GetRolesHandler(_roleRepository.Object, serviceProvider.GetService<IMapper>());
        }
        [Fact]

        public void Handle_Returns_All_RoleDto()
        {
            //Arrange
            List<RoleDto> compareResult = new List<RoleDto> 
            {
                new RoleDto
                {
                    Id = new Guid("2ec1af40-5a2d-4e1d-8739-b9b60752482d"),
                    Name = "Admin",
                    Description = "I'm admin. I need your MAC"
                },
                new RoleDto
                {
                    Id = new Guid("b73b6bd3-e516-4afd-9d68-be8f5180bd62"),
                    Name = "User",
                    Description = "I need help"
                }
            };
             
            //Act
            var roles = _handler.Handle(new GetRolesAsyncQuery(), new CancellationToken()).Result.ToList();
            //Assert
            Assert.Collection(compareResult,
                e =>
                {
                    Assert.Equal(e.Id, roles[0].Id);
                    Assert.Equal(e.Name, roles[0].Name);
                    Assert.Equal(e.Description, roles[0].Description);
                },
                e =>
                {
                    Assert.Equal(e.Id, roles[1].Id);
                    Assert.Equal(e.Name, roles[1].Name);
                    Assert.Equal(e.Description, roles[1].Description);
                }
                );
        }
    }
}
