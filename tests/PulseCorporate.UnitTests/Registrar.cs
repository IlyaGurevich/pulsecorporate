﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PulseCorporate.Application.Interfaces;
using PulseCorporate.Application.Mapping;
using PulseCorporate.Infrastructure.Persistence;
using PulseCorporate.Repositories.Abstractions.Administration;
using PulseCorporate.Repositories.Implementation.Administration;
using PulseCorporate.WebUI.Mapping;

namespace tests.PulseCorporate.UnitTests
{
    internal static class Registrar
    {
        internal static IServiceCollection AddServicesTests_Mock(this IServiceCollection services, IConfiguration configuration)
        {
            return services.ConfigureAutomapper();
        }

        internal static IServiceCollection AddServicesTests_InMemory(this IServiceCollection services, IConfiguration configuration)
        {
            return services
                .ConfigureAutomapper()
                .AddInfrastructureServices_InMemoryContext(configuration)
                .InstallRepositories();
        }

        public static IServiceCollection AddInfrastructureServices_InMemoryContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IApplicationContext>(provider => provider.GetRequiredService<PulseCorporateContext>());
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();  
            services.AddDbContext<PulseCorporateContext>(options =>
            {
                options.UseInMemoryDatabase("InMemoryDb", builder => { });
                options.UseInternalServiceProvider(serviceProvider);
            });
            services.AddTransient<DbContext, PulseCorporateContext>();         
            return services;
        }

        private static IServiceCollection InstallRepositories(this IServiceCollection serviceCollection)
        {
            serviceCollection
                .AddTransient<IRoleRepository, RoleRepository>();
            return serviceCollection;
        }
        private static IServiceCollection ConfigureAutomapper(this IServiceCollection services) => services
           .AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()));

        private static MapperConfiguration GetMapperConfiguration()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                //role
                cfg.AddProfile<RoleProfile>();
                cfg.AddProfile<RoleUiProfile>();
            });
            configuration.AssertConfigurationIsValid();
            return configuration;
        }
    }
}
