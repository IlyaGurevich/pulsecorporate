import axios from 'axios';

export const createAxiosInstance = (keycloak) => {
	if (!keycloak || !keycloak.token) {
		throw new Error('Keycloak instance and token are required to create Axios instance');
	}

	const axiosInstance = axios.create({
		baseURL: process.env.REACT_APP_BACKEND_API_URL,
		headers: {
			Authorization: `Bearer ${keycloak.token}`
		}
	});

	const setAxiosAuthHeader = () => {
		if (keycloak.isTokenExpired(5)) {
			keycloak.updateToken(30).then(refreshed => {
				if (refreshed) {
					console.log('Token was successfully refreshed');
					axiosInstance.defaults.headers['Authorization'] = `Bearer ${keycloak.token}`;
				}
			}).catch(error => {
				console.error('Failed to refresh token', error);
			});
		}
	};

	setAxiosAuthHeader();

	const intervalId = setInterval(setAxiosAuthHeader, 60000);

	axiosInstance.cancelTokenRefresh = () => clearInterval(intervalId);

	return axiosInstance;
};