
// import axios from "axios";


// export const getPosts = async (page, limit) => {
// 	try {
// 		const response = await axios.get(`${process.env.REACT_APP_BACKEND_API_URL}/Posts?page=${page}&limit=${limit}`);
// 		return response.data;
// 	} catch (error) {
// 		console.error('There was an error!', error);
// 		return [];
// 	}
// };

// PostService.js
import axios from "axios";

export const axiosInstance = axios.create({
	baseURL: process.env.REACT_APP_BACKEND_API_URL,
});

export const getPosts = async (page, limit) => {
	try {
		const response = await axiosInstance.get(`/Posts?page=${page}&limit=${limit}`);
		return response.data;
	} catch (error) {
		console.error('There was an error!', error);
		return [];
	}
};
