"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.publicRoutes = exports.privateRoutes = void 0;

var _Home = _interopRequireDefault(require("../pages/Home"));

var _Error = _interopRequireDefault(require("../pages/Error"));

var _Support = _interopRequireDefault(require("../pages/Support"));

var _Calendar = require("../pages/Calendar");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var privateRoutes = [{
  path: "/support",
  component: _Support["default"],
  exact: true
}, //{ path: "/about", component: About, exact: true },
//{ path: "/roles", component: Roles, exact: true },
//{ path: "/roles/:id", component: PostIdPage, exact: true },
{
  path: "/calendar",
  component: _Calendar.CalendarPage,
  exact: true
}, {
  path: "/home",
  component: _Home["default"],
  exact: true
}, {
  path: "/",
  component: _Home["default"],
  exact: true
}, {
  path: "/error",
  component: _Error["default"],
  exact: true
}, {
  path: "/*",
  component: _Error["default"],
  exact: true
}];
exports.privateRoutes = privateRoutes;
var publicRoutes = [{
  path: "/",
  component: _Home["default"],
  exact: true
}, //{ path: "/signin", component: SignIn, exact:true},
//{ path: "/signup", component: SignUp, exact: true },
{
  path: "/support",
  component: _Support["default"],
  exact: true
}, {
  path: "/*",
  component: _Home["default"],
  exact: true
}];
exports.publicRoutes = publicRoutes;