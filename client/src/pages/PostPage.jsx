import React, { useState, useEffect } from "react";
import { Box, Modal, Fade, Backdrop, TextField, Button } from "@mui/material";

export const PostModal = ({ open, handleClose, handleSubmit, initialPost }) => {
  const [id, setEditableId] = useState(initialPost?.id || null);
  const [name, setEditableName] = useState(initialPost?.name || "");
  const [description, setEditabledDscription] = useState(
    initialPost?.description || ""
  );
  const [createdDate, setEditableCreatedDate] = useState(
    initialPost?.createdDate || Date.now()
  );

  useEffect(() => {
    setEditableId(initialPost?.id || null);
    setEditableName(initialPost?.name || "");
    setEditabledDscription(initialPost?.description || "");
    setEditableCreatedDate(initialPost?.createdDate || Date.now());
  }, [initialPost]);

  return (
    <Modal
      open={open}
      onClose={handleClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={open}>
        <Box sx={{ ...modalStyle }}>
          <h2>{initialPost ? "Edit Post" : "Create Post"}</h2>
          <TextField
            margin="dense"
            name="title"
            label="Title"
            fullWidth
            value={name}
            onChange={(e) => setEditableName(e.target.value)}
          />
          <TextField
            margin="dense"
            name="content"
            label="Content"
            fullWidth
            multiline
            rows={4}
            value={description}
            onChange={(e) => setEditabledDscription(e.target.value)}
          />
          <Button
            onClick={() =>
              handleSubmit({
                id,
                name,
                description,
                createdDate,
              })
            }
            color="primary"
          >
            Submit
          </Button>
          <Button onClick={handleClose}>Cancel</Button>
        </Box>
      </Fade>
    </Modal>
  );
};

const modalStyle = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4,
};
