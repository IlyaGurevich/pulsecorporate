import React from "react";
import { Card, CardContent, Typography, Box, Link } from "@mui/material";
import TelegramIcon from "@mui/icons-material/Telegram";

const SupportPage = () => {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        height: "100vh",
        p: 3,
      }}
    >
      <Card sx={{ minWidth: 275, boxShadow: 3 }}>
        <CardContent
          sx={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            p: 4,
          }}
        >
          <TelegramIcon color="primary" sx={{ fontSize: 40, mb: 2 }} />
          <Typography variant="h5" component="div" sx={{ mb: 2 }}>
            Техническая Поддержка
          </Typography>
          <Typography variant="subtitle1" sx={{ textAlign: "center" }}>
            Если у вас возникли вопросы или проблемы, свяжитесь с нашими
            специалистами в Telegram:
          </Typography>
          <Box
            sx={{
              mt: 2,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Link
              href="https://t.me/V_A_GNAT"
              color="secondary"
              target="_blank"
              sx={{ mb: 1 }}
            >
              @V_A_GNAT
            </Link>
            <Link
              href="https://t.me/gurevich_ilya"
              color="secondary"
              target="_blank"
            >
              @gurevich_ilya
            </Link>
          </Box>
        </CardContent>
      </Card>
    </Box>
  );
};

export default SupportPage;
