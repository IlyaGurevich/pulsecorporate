import React from "react";
import { Box, Typography, Button } from "@mui/material";
import { useNavigate } from "react-router-dom"; // Используйте useNavigate вместо useHistory

const NotFoundPage = () => {
  const navigate = useNavigate(); // Это для переадресации

  const goHome = () => {
    navigate("/"); // Перенаправить пользователя на главную страницу
  };

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        height: "100vh",
        textAlign: "center",
        p: 3,
      }}
    >
      <Typography variant="h3" component="h1" gutterBottom>
        Ой! Страница не найдена.
      </Typography>
      <Typography variant="subtitle1" color="textSecondary" gutterBottom>
        Извините, мы не смогли найти запрашиваемую страницу.
      </Typography>
      <Box mt={4}>
        <Button variant="contained" color="primary" onClick={goHome}>
          Вернуться на главную
        </Button>
      </Box>
    </Box>
  );
};

export default NotFoundPage;
