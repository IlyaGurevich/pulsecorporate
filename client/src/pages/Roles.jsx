import React, {useEffect, useState} from 'react'
import RoleService from '../API/RoleService';
import PostService from '../API/PostService';
;


function Roles() {
  const [listRoles, setListRoles] = useState([]);

  useEffect(() => {
    const getRoles = async () => {
      const roles = await RoleService.getAll();
      setListRoles(roles?.data?.map((role) => <li>{role.name}</li>));
    }

    getRoles();
  }, [])

  return (
    listRoles.length !== 0 ? <ul>{listRoles}</ul> : "Empty"
  )
}
  export default Roles