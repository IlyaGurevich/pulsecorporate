import React, { useState, useEffect } from "react";
import { Box, CircularProgress } from "@mui/material";
import { useInView } from "react-intersection-observer";
import { TransitionGroup, CSSTransition } from "react-transition-group";
//import { getPosts } from "../API/PostService";

import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import IconButton from "@mui/material/IconButton";
import { createAxiosInstance } from "../API/CreateAxios";
import { useKeycloak } from "@react-keycloak/web";
import { Post } from "../Components/PostItem";
import { PostModal } from "./PostPage";

const PostsPage = () => {
  const { keycloak, initialized } = useKeycloak();
  const axiosInstance = createAxiosInstance(keycloak);
  const [posts, setPosts] = useState([]);
  const [page, setPage] = useState(1);
  const { ref, inView } = useInView();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [editingPost, setEditingPost] = useState(null);

  const fetchPosts = async () => {
    const response = await axiosInstance.get(`/Posts?page=${page}&limit=10`);
    const newPosts = response.data;
    setPage((prevPage) => prevPage + 1);
    setPosts((prevPosts) => [...prevPosts, ...newPosts]);
  };

  const handleSubmitPost = async (post) => {
    const method = post.id ? "put" : "post";
    const endpoint = post.id ? `/Posts/${post.id}` : "/Posts";

    if (post.id) {
      const postBackPut = await axiosInstance[method](endpoint, post);
      setPosts(posts.map((p) => (p.id === post.id ? postBackPut.data : p)));
    } else {
      const { id, ...newPost } = post;
      newPost.createdDate = new Date(newPost.createdDate).toISOString();
      const postBack = await axiosInstance[method](endpoint, newPost);
      setPosts([postBack.data, ...posts]);
    }

    setIsModalOpen(false);
  };

  useEffect(() => {
    if (!axiosInstance) return;
    fetchPosts();
  }, []);

  useEffect(() => {
    if (inView) {
      fetchPosts();
    }
  }, [inView]);

  const handleEditPost = (post) => {
    setEditingPost(post);
    setIsModalOpen(true);
    console.log("Edit post", post.Id);
  };

  const handleDeletePost = async (postId) => {
    await axiosInstance.delete(`/Posts/${postId}`);

    setPosts(posts.filter((post) => post.id !== postId));
    console.log("Delete post", postId);
  };

  const handleCreatePost = () => {
    setEditingPost(null);
    setIsModalOpen(true);
    console.log("Create new post");
  };

  return (
    <Box sx={{ p: 3 }}>
      <Box display="flex" justifyContent="flex-end" mb={2}>
        <IconButton color="primary" onClick={handleCreatePost}>
          <AddCircleOutlineIcon fontSize="large" />
        </IconButton>
      </Box>
      <TransitionGroup>
        {posts.map((post, index) => (
          <CSSTransition key={post.id} timeout={500} classNames="my-done-enter">
            <Post
              key={index}
              {...post}
              onEdit={() => handleEditPost(post)}
              onDelete={() => handleDeletePost(post.id)}
            />
          </CSSTransition>
        ))}
      </TransitionGroup>
      <div ref={ref}>
        <CircularProgress />
      </div>
      <PostModal
        open={isModalOpen}
        handleClose={() => {
          setIsModalOpen(false);
          setEditingPost(null);
        }}
        handleSubmit={handleSubmitPost}
        initialPost={editingPost}
      />
    </Box>
  );
};

export default PostsPage;
