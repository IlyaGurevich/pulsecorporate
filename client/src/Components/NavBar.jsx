import React, { useCallback, useState, useEffect } from "react";
import {
  AppBar,
  Toolbar,
  Typography,
  Button,
  Box,
  Dialog,
  DialogTitle,
  DialogContent,
  TextField,
  DialogActions,
} from "@mui/material";
import { useKeycloak } from "@react-keycloak/web";
import { Link as RouterLink } from "react-router-dom";
import IconButton from "@mui/material/IconButton";
import LogoutIcon from "@mui/icons-material/Logout";
import { createAxiosInstance } from "../API/CreateAxios";

const NavBar = () => {
  const { keycloak, initialized } = useKeycloak();
  const [userProfile, setUserProfile] = useState(null);
  const [profileOpen, setProfileOpen] = useState(false);
  const [editableName, setEditableName] = useState("");
  const [editableSurname, setEditableSurname] = useState("");
  const [email, setEmail] = useState("");

  const login = useCallback(() => {
    keycloak?.login();
  }, [keycloak]);

  const logout = useCallback(() => {
    keycloak?.logout();
  }, [keycloak]);

  const fetchUserProfile = async () => {
    const axiosInstance = createAxiosInstance(keycloak);
    const response = await axiosInstance.get(`/Users/CurrentUser`);
    const userData = response.data;
    setUserProfile(userData);
  };

  const handleSaveChanges = async () => {
    const axiosInstance = createAxiosInstance(keycloak);
    try {
      await axiosInstance.put(`/Users/SetUser`, {
        name: editableName,
        surname: editableSurname,
      });
      handleClose();
    } catch (error) {
      console.error("There was an error updating the user profile", error);
    }
  };

  useEffect(() => {
    if (profileOpen) {
      fetchUserProfile();
    }
  }, [profileOpen]);

  useEffect(() => {
    if (userProfile) {
      setEditableName(userProfile.name);
      setEditableSurname(userProfile.surname);
      setEmail(userProfile.email);
    }
  }, [userProfile]);

  const handleProfileClick = () => {
    setProfileOpen(true);
  };

  const handleClose = () => {
    setProfileOpen(false);
  };

  if (!initialized) {
    return <></>;
  }

  return (
    <>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            Pulse corporate
          </Typography>
          <Box sx={{ display: "flex", gap: 2 }}>
            <Button color="inherit" component={RouterLink} to="/">
              Home
            </Button>
            {/* <Button color="inherit" component={RouterLink} to="/about">
            About
          </Button> */}
            <Button color="inherit" component={RouterLink} to="/support">
              Support
            </Button>

            {!!keycloak.authenticated ? (
              <div>
                <Button color="inherit" component={RouterLink} to="/calendar">
                  Calendar
                </Button>
                <Button
                  color="primary"
                  variant="contained"
                  onClick={handleProfileClick}
                >
                  Profile
                </Button>
                <IconButton
                  color="inherit"
                  onClick={logout}
                  aria-label="logout"
                >
                  <LogoutIcon />
                </IconButton>
              </div>
            ) : (
              <Button onClick={login} color="secondary" variant="contained">
                Login
              </Button>
            )}
          </Box>
        </Toolbar>
      </AppBar>
      <Dialog open={profileOpen} onClose={handleClose}>
        <DialogTitle>Edit Profile</DialogTitle>
        <DialogContent>
          <TextField
            margin="dense"
            label="Name"
            type="text"
            fullWidth
            variant="standard"
            value={editableName}
            onChange={(e) => setEditableName(e.target.value)}
          />
          <TextField
            margin="dense"
            label="Surname"
            type="text"
            fullWidth
            variant="standard"
            value={editableSurname}
            onChange={(e) => setEditableSurname(e.target.value)}
          />
          <TextField
            disabled
            margin="dense"
            label="E-mail"
            type="text"
            fullWidth
            variant="standard"
            value={email}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={handleSaveChanges} color="primary">
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};
export default NavBar;
