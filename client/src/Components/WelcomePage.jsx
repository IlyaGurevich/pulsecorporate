import React from "react";
import { Typography, Paper, Box, Container, CssBaseline } from "@mui/material";
import { createTheme, ThemeProvider } from "@mui/material/styles";

// Создание темы для настройки цветовой палитры и типографики
const theme = createTheme({
  palette: {
    primary: {
      main: "#556cd6",
    },
    secondary: {
      main: "#19857b",
    },
  },
  typography: {
    h1: {
      fontSize: "2.2rem",
      fontWeight: "500",
      marginBottom: "0.35em",
    },
    h2: {
      fontSize: "1.8rem",
      fontWeight: "500",
      marginTop: "0.35em",
    },
  },
});

const WelcomePage = () => {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Container component="main" maxWidth="md">
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            marginTop: 8,
            marginBottom: 2,
          }}
        >
          <Paper
            elevation={6}
            sx={{
              width: "100%",
              padding: 4,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: (theme) => theme.palette.background.default,
              color: (theme) => theme.palette.text.primary,
            }}
          >
            <Typography variant="h1" color="primary" gutterBottom>
              Добро пожаловать!
            </Typography>
            <Typography variant="h2" color="secondary" gutterBottom>
              Ваша новая работа начинается здесь.
            </Typography>
            <Typography
              variant="body1"
              color="textPrimary"
              align="center"
              sx={{ mt: 3, mb: 4 }}
            >
              Исследуйте возможности, взаимодействуйте с коллегами и получайте
              уникальный опыт с нашим сервисом.
            </Typography>
          </Paper>
        </Box>
      </Container>
    </ThemeProvider>
  );
};

export default WelcomePage;
