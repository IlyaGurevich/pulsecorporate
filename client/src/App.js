import { BrowserRouter, Route, Routes } from "react-router-dom";
import React, { useEffect, useState } from "react";
import NavBar from "./Components/NavBar";
import AppRouter from "./Components/AppRouter";
import { AuthContext } from "./context";
import keycloak from "./identity/keycloak";
import { ReactKeycloakProvider } from '@react-keycloak/web';
import "bootstrap/dist/css/bootstrap.min.css"

function App() {

	const eventLogger = (event, error) => {
		console.log('onKeycloakEvent', event, error);
	};

	const tokenLogger = (tokens) => {
		console.log('onKeycloakTokens', tokens);
	};

	// const [isAuth, setIsAuth] = useState(false);
	// const [isLoading, setLoading] = useState(true);

	// useEffect(() => {
	//   if (localStorage.getItem("auth")) {
	//     setIsAuth(true);
	//   }
	//   setLoading(false);
	// }, []);
	console.log(keycloak);
	return (
		// <AuthContext.Provider
		//   value={{
		//     isAuth,
		//     setIsAuth,
		//     isLoading,
		//   }}
		// >
		<>
			<ReactKeycloakProvider
				authClient={keycloak}
				onEvent={eventLogger}
				onTokens={tokenLogger}
				initOptions={{
					pkceMethod: 'S256',
					checkLoginIframe: true
				}}
			>
				{/* <React.StrictMode> */}
				<BrowserRouter>
					<NavBar />
					<AppRouter />
				</BrowserRouter>
				{/* </React.StrictMode> */}
				{/* </AuthContext.Provider> */}
			</ReactKeycloakProvider>
		</>
	);
}
export default App;
